package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.beans.Pizza;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Form;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());

    @Override
    protected Application configure() {
        return new ApiV1();
    }
    private PizzaDao dao;

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
    // et les DAO

    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
        dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
        dao.dropTable();
    }

    @Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


        // On vérifie la valeur retournée (liste vide)
        // L'entité (readEntity() correspond au corps de la réponse HTTP.
        // La classe javax.ws.rs.core.GenericType<T> permet de définir le type
        // de la réponse lue quand on a un type complexe (typiquement une liste).
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});

        assertEquals(0, pizzas.size());

    }

    @Test
    public void testGetExistingPizza() {
        Pizza pizza = new Pizza();
        pizza.setName("mozzarella");

        long id = dao.insert(pizza.getName());
        pizza.setId(id);

        Response response = target("/pizzas/" + id).request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza, result);

    }

    @Test
    public void testGetNotExistingPizza() {
        Response response = target("/pizzas/125").request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }

    @Test
    public void testCreatePizza() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("mozzarella");

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        // On vérifie le code de status à 201
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        // On vérifie que le champ d'entête Location correspond à
        // l'URI de la nouvelle entité
        assertEquals(target("/pizzas/" +
                returnedEntity.getId()).getUri(), response.getLocation());

        // On vérifie que le nom correspond
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }

    @Test
    public void testCreateSamePizza() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("mozzarella");
        dao.insert(pizzaCreateDto.getName());

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteExistingPizza() {
        Pizza pizza = new Pizza();
        pizza.setName("mozzarella");
        long id = dao.insert(pizza.getName());
        pizza.setId(id);

        Response response = target("/pizzas/" + id).request().delete();

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Pizza result = dao.findById(id);
        assertEquals(result, null);
    }

    @Test
    public void testDeleteNotExistingPizza() {
        Response response = target("/pizzas/125").request().delete();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
                response.getStatus());
    }

    @Test
    public void testGetPizzaName() {
        Pizza pizza = new Pizza();
        pizza.setName("mozzarella");
        long id = dao.insert(pizza.getName());

        Response response = target("pizzas/" + id + "/name").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        assertEquals("mozzarella", response.readEntity(String.class));
    }

    @Test
    public void testGetNotExistingPizzaName() {
        Response response = target("pizzas/125/name").request().get();

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "chorizo");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        long id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
        Pizza result = dao.findById(id);

        assertNotNull(result);
    }

}
