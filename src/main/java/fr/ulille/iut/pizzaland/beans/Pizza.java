package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;

import java.util.List;


public class Pizza {
    private long id;
    private String name;
    private List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(long id, String name, List<Ingredient> ingredients) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
    }

    public Pizza(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getListIngredients() {
        return this.ingredients;
    }

    public void setListIngredients(List<Ingredient> liste) {
        this.ingredients = liste;
    }

    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setListIngredients(p.getListIngredients());
        return dto;
    }

    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());
        pizza.setListIngredients(dto.getListIngredients());
        return pizza;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pizza other = (Pizza) obj;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + ", ingrédients=" + ingredients.toString() + "]";
    }

    public static PizzaCreateDto toCreateDto(Pizza piz) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(piz.getName());

        return dto;
    }

    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());

        return pizza;
    }
}