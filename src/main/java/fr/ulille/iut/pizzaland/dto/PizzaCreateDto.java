package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

import java.util.List;

public class PizzaCreateDto {
    private String name;
    private List<Ingredient> ingredients;

    public PizzaCreateDto() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getListIngredients() {
        return this.ingredients;
    }

    public void setListIngredients(List<Ingredient> liste) {
        this.ingredients = liste;
    }
}