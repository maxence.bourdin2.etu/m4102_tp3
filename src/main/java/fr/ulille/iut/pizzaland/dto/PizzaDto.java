package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

import java.util.List;

public class PizzaDto {
    private long id;
    private String name;
    private List<Ingredient> ingredients;

    public PizzaDto() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getListIngredients() {
        return this.ingredients;
    }

    public void setListIngredients(List<Ingredient> liste) {
        this.ingredients = liste;
    }

}